require_relative "boot"

require "rails/all"

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module HealthDiet
  class Application < Rails::Application
    config.load_defaults 6.1

    config.before_configuration do
      env_file = File.join(Rails.root, 'config', 'config.yml')

      if !File.exists? env_file
        if ENV['DEVELOPMENT']
          env_file = File.join(Rails.root, 'config', 'config.development.yml')
        elsif ENV['DOCKER']
          env_file = File.join(Rails.root, 'config', 'config.docker.yml')
        else
          env_file = File.join(Rails.root, 'config', 'config.default.yml')
        end
      end

      YAML.load(File.open(env_file)).each do |key, value|
        ENV[key.to_s] = value
      end
    end
    
    config.time_zone = "Central Time (US & Canada)"
    config.time_zone = 'America/Fortaleza'
    config.i18n.default_locale = "pt-BR"
    config.encoding = "utf-8"
    # config.eager_load_paths << Rails.root.join("extras")
  end
end
